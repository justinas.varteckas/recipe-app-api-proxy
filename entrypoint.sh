#!/bin/sh
# Entrypoint script to run application inside docker
set -e # Output errors

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf # Populates conf with values from system ENV and outputs to new file

nginx -g 'daemon off;' # In docker run in foreground


