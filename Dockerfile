# Nginx image
FROM nginxinc/nginx-uprivileged:1-alpine
LABEL maintainer="Justin co."

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uswgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root #switch to root user

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh #set as executable file

USER nginx

CMD ["/entrypoint.sh"]
# TODO build this image